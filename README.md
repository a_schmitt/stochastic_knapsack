# Logistic Knapsack

See
[link](https://docs.google.com/document/d/14K72TC_o6jKs8IdIVbdvtd7akTM9Qor71GCmH9xNxnY/edit#heading=h.cnxgos1wjvys)
for a problem description.

## Algorithm

If the volumes v_i for each item i are exactly know, we obtain the standard
Knapsack problem

min c^t x
s.t.
v^t x <= d
x in {0, 1}^m.

However, v in R^m is unknown, except for the knowledge that A v = y.
Unfortunately y is unknown (and A might not even be regular) but we know f given
by f = y + e for some independent N(0, 2) random variables.

Note, that the description of the problem is given using volumes, but it is
explicitly stated that the sum of volumes is measured, i.e., we can assume the
volumes to be one dimensional and not care about the correct modeling in 3d
space :)

### Least Squares Solution

The natural solution in this setting (looking at the normal distributed errors)
is to just take the least squares solution u to A v = f and plug it into the
knapsack.

This gives a point estimate of v, a knapsack solution for u could be easily
infeasible for v. A more involved approach is to take the variance of the least
squares solution into account. Using the information about the variance of e
this can be easily expressed, see
[here](https://people.math.ethz.ch/~geer/bsa199_o.pdf) for an example. This
allows us to derive a confidence interval [s_i, t_i] for v_i. Taking the
pessimistic approach that we sacrifice optimality in favor of feasibility we
thus solve the knapsack with t instead of u.

### Robust (Distribution Agnostic) Optimization Approach

I guess a general approach in the way of robust optimization is to assume, that
a suitable candidate set of volumes V \subset \R^m is known and we want to find
the optimal selections of items which are feasible for all these volumes.

This would be formulated as

min c^t x
s.t.
max_{u \in V} u^t x <= d
x in {0, 1}^m.

I first experimented with the idea to put V = {u in R^m | A u = f, u >= 0}.
This, did not perform very well, since the chance of A u = f being unsolvable
is high. Then V = {} and the solution to the robust optimization is the for sure
unwanted is x = (1, ..., 1).

To remedy this a bit I find the minimal r in R_+ such that V = {u in R^m | f - r
<= A u <= f + r, u >= 0} has a solution. Using duality theory the robust
optimization problem has the robust counterpart

min c^t x
s.t.
(f+r)^t h + (f-r)^t g <= 0
A^t (h + g) >= x
h >= 0, g <= 0
x in {0, 1}^m,

which can be easily solved using MIP software.

### Evaluation

We have implemented two different types of random instance generators. We
randomly generate A, c, v and the error E. The uniform strategy, basically has A
randomly filled with ones. Poisson tries to mirror the example a bit by sampling
the number of packages an item belongs to via a Poisson(68) distribution, as
suggested in a first data exploratory.

The evaluation on 100 instances per instance type is mixed. The solving time of
the robust approach is quite high. Interestingly also the least squares with
confidence intervals takes a bit more time, since it needs to calculate an
inverse of a #items x #items matrix. Naturally the least squares approach with
confidence intervals leads to more feasible solutions compared to the default
least squares approach. The robust approach is as robust and also leads to
always feasible solutions. Furthermore, the solution quality is better than the
least squares with CI approach, nearly reaching the default CI approach. The
trade-off is the high computational cost. Note, that we took the 0.95 confidence
interval, which is quite conservative. It might be that one gets a better
feasible to gap trade-off by choosing a smaller confidence interval.


| name    | solver   |   feasible |       gap |    time |
|:--------|:---------|-----------:|----------:|--------:|
| uniform | ls       |       0.96 | 0.0471088 | 1.40824 |
| uniform | ls_ci    |       1    | 0.0187235 | 10.8312 |
| uniform | robust   |       1    | 0.0571095 | 106.833 |
| poisson | ls       |       0.96 | 0.0471088 | 1.41801 |
| poisson | ls_ci    |       1    | 0.0187235 | 14.3506 |
| poisson | robust   |       1    | 0.0571095 | 119.149 |

*feasible = fraction of solutions, which are feasible if the correct volumes v
are used
*gap = average of (optimal_solution_value - solution_value) /
optimal_solution_value) where the optimal_solution_value is computed using the
knowledge of the correct volumes v
*time = total time to solve the 100 instances in seconds on my working machine

## Local Setup

### Poetry environment
Fork the repository and set up a poetry environment with Python 3.10 by executing
```bash
poetry env use 3.10 && poetry install
```
This command assumes that you have [poetry](https://python-poetry.org/) installed on your machine.


### Files
* the main code is in `logistic_knapsack` folder.
* the `solver` subfolder implements the 3 solvers.
* the `knapsack_solver` is used to get the correct value for a vanilla knapsack problem.
* `random_test_instances.py` implements code to get test instances.
* `instance_evaluation.py` runs a script to compare the solvers on the test instances.
* `obtain_solutions_to_example.py` computes solutions to the example problem using the three algorithms.
* `notebooks/exploration.ipynb` explores a bit the structure of the example instance.
