from solver.least_squares_solver import LeastSquaresSolver, LeastSquaresWithCISolver
from solver.robust_solver import RobustSolver

for solver in [LeastSquaresSolver(), LeastSquaresWithCISolver(), RobustSolver()]:
    print(f"{solver}: {solver.solve_example(max_volume=40)}")
