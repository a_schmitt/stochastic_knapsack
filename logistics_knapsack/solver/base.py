import json

import numpy as np
import pandas as pd
from random_test_instance import RandomTestInstance


class Solver:
    def solve(self, instance: RandomTestInstance, max_volume: int):
        pass

    def solve_example(self, max_volume: int):
        with open("../example_data/packages.json", "r") as f:
            data_packages = json.load(f)

        with open("../example_data/items.json", "r") as f:
            data_item_price = json.load(f)

        item_price_df = pd.DataFrame(data_item_price).set_index("name")
        package_df = pd.DataFrame(data_packages)
        package_item_df = package_df.explode("items")
        package_item_df["package_id"] = package_item_df.index
        package_item_df["in_package"] = 1
        x = package_item_df.pivot_table(
            index="package_id", columns="items", values="in_package", fill_value=0
        ).sort_index()  # .to_numpy()
        indice_to_item_order = x.columns.copy()
        x = x.to_numpy()

        n_packages, n_items = x.shape
        instance = RandomTestInstance(n_items, n_packages)

        instance.item_volumes = np.zeros(n_items)
        instance.item_prices = item_price_df.loc[indice_to_item_order].to_numpy()
        instance.packages = x
        instance.package_volumes = np.zeros(n_packages)
        instance.measured_package_volumes = package_df.sort_index()[
            "total_volume"
        ].to_numpy()
        obj, solution, expected_total_volume = self.solve(instance, max_volume)

        return (
            obj,
            indice_to_item_order.to_numpy()[np.array(solution, dtype=bool)],
            expected_total_volume,
        )
