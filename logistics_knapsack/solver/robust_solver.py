import highspy
import numpy as np
from random_test_instance import RandomTestInstance, solve_knapsack_highs
from solver.base import Solver


class RobustSolver(Solver):
    def compute_worst_case_total_volume(
        self, instance, solution: np.array, relaxation_factor: float
    ):
        y = instance.measured_package_volumes
        x = instance.packages

        model = highspy.Highs()
        model.setOptionValue("output_flag", False)
        model.setOptionValue("random_seed", 100)
        model.changeObjectiveSense(highspy.ObjSense.kMaximize)

        n, m = x.shape
        model.addVars(m, [0] * m, [highspy.kHighsInf] * m)

        model.changeColsCost(m, np.arange(m), solution)

        row_starts = np.arange(n) * m
        indices = np.repeat(np.atleast_2d(np.arange(m)), n, axis=0)
        coefficients = x.copy()

        lb = (y - relaxation_factor).clip(min=0)
        ub = y + relaxation_factor
        model.addRows(
            n, lb, ub, n * m, row_starts, indices.flatten(), coefficients.flatten()
        )

        model.run()

        return model.getInfoValue("objective_function_value")[1]

    def compute_minimal_relaxation_factor(self, instance):
        # find the minimal relaxation factor such that there exists a feasible solution for the antagonist
        # min r
        # s.t.
        # A v <= f + r, A v >= f - r, v >= 0

        f = instance.measured_package_volumes
        A = instance.packages

        model = highspy.Highs()
        model.setOptionValue("output_flag", False)
        model.setOptionValue("random_seed", 100)
        model.changeObjectiveSense(highspy.ObjSense.kMinimize)

        n, m = A.shape
        model.addVars(m + 1, [0] * (m + 1), [highspy.kHighsInf] * (m + 1))

        model.changeColsCost(1, m, 1)

        row_starts = np.arange(2 * n) * (m + 1)
        indices = np.repeat(np.atleast_2d(np.arange(m + 1)), 2 * n, axis=0)
        coefficients_1 = np.concatenate((A, -np.ones(n)[:, None]), axis=1)
        coefficients_2 = np.concatenate((A, np.ones(n)[:, None]), axis=1)
        coefficients = np.concatenate((coefficients_1, coefficients_2), axis=0)
        lb = np.concatenate(([-highspy.kHighsInf] * n, f))
        ub = np.concatenate((f, [highspy.kHighsInf] * n))
        model.addRows(
            2 * n,
            lb,
            ub,
            2 * n * (m + 1),
            row_starts,
            indices.flatten(),
            coefficients.flatten(),
        )

        model.run()

        return model.getInfoValue("objective_function_value")[1]

    def solve(self, instance: RandomTestInstance, maxVolume):
        # solve
        # max c^t x
        # s.t.
        # v^t x <= maxVolume forall v in V
        # x in {0, 1}^m
        # where V = {v | A v <= f + r, A v >= max(f - r, 0), v >= 0}
        # the constraints are equivalent to
        # lambda^t * (f +e) + gamma^t * (f - e) <= maxVolume
        # A^T lambda + A^T gamma >= x
        # lambda >= 0 gamma <= 0

        relaxation_factor = max(self.compute_minimal_relaxation_factor(instance), 1)
        relaxation_factor *= 1.1

        f = instance.measured_package_volumes
        A = instance.packages

        model = highspy.Highs()
        model.setOptionValue("output_flag", False)
        model.setOptionValue("random_seed", 100)
        # add timelimit sometime
        model.changeObjectiveSense(highspy.ObjSense.kMaximize)

        n, m = A.shape
        model.addVars(
            m + 2 * n,
            [0] * m + [0] * n + [-highspy.kHighsInf] * n,
            [1] * m + [highspy.kHighsInf] * n + [0] * n,
        )

        model.changeColsCost(m, np.arange(m), instance.item_prices)
        model.changeColsIntegrality(
            m, np.arange(m), np.array([highspy.HighsVarType.kInteger] * m)
        )

        coefficients = np.concatenate(
            (f + relaxation_factor, (f - relaxation_factor).clip(min=0))
        )
        model.addRows(
            1,
            -highspy.kHighsInf,
            maxVolume,
            2 * n,
            [0],
            np.arange(2 * n) + m,
            coefficients,
        )

        row_starts = np.arange(m) * (2 * n + 1)
        indices = np.repeat(np.atleast_2d(np.arange(2 * n) + m), m, axis=0)
        indices = np.concatenate((indices, np.arange(m)[:, None]), axis=1)
        coefficients = np.concatenate((A.T, A.T, -np.ones(shape=(m, 1))), axis=1)
        ub = [highspy.kHighsInf] * m
        lb = [0] * m
        model.addRows(
            m,
            lb,
            ub,
            coefficients.size,
            row_starts,
            indices.flatten(),
            coefficients.flatten(),
        )

        model.run()

        solution = model.getSolution()
        vals = solution.col_value[:m].copy()

        total_volume = self.compute_worst_case_total_volume(
            instance, vals, relaxation_factor
        )

        return model.getInfoValue("objective_function_value")[1], vals, total_volume
