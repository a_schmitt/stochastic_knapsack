import json
from abc import ABC

import numpy as np
import pandas as pd
from numpy.linalg import LinAlgError
from random_test_instance import RandomTestInstance, solve_knapsack_highs
from sklearn.linear_model import LinearRegression
from solver.base import Solver


class LeastSquaresSolver(Solver):
    def solve(self, instance: RandomTestInstance, max_volume: int):
        y = instance.measured_package_volumes
        x = instance.packages
        model = LinearRegression(fit_intercept=False)
        model.fit(x, y)

        estimated_item_volumes = model.coef_.copy()

        obj, solution = solve_knapsack_highs(
            estimated_item_volumes, instance.item_prices, max_volume
        )

        return obj, solution, estimated_item_volumes @ solution


class LeastSquaresWithCISolver(Solver):
    def solve(self, instance: RandomTestInstance, max_volume: int):
        y = instance.measured_package_volumes
        x = instance.packages
        model = LinearRegression(fit_intercept=False)
        model.fit(x, y)

        estimated_item_volumes = model.coef_.copy()
        try:
            estimated_item_volumes += np.diag(np.linalg.inv((x.T @ x))) * 2
        except LinAlgError:
            # Singular matrix, just use pseudo inverse
            estimated_item_volumes += np.diag(np.linalg.pinv((x.T @ x))) * 2
        obj, solution = solve_knapsack_highs(
            estimated_item_volumes, instance.item_prices, max_volume
        )

        return obj, solution, estimated_item_volumes @ solution
