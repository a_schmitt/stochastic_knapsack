import highspy
import numpy as np


def solve_knapsack_highs(weights, costs, max_weight):
    n_items = len(weights)

    model = highspy.Highs()
    model.setOptionValue("output_flag", False)
    model.setOptionValue("random_seed", 100)
    model.changeObjectiveSense(highspy.ObjSense.kMaximize)

    model.addVars(n_items, [0] * n_items, [1] * n_items)

    indices = np.arange(n_items)
    model.changeColsCost(n_items, indices, costs)
    model.changeColsIntegrality(
        n_items, indices, np.array([highspy.HighsVarType.kInteger] * n_items)
    )

    model.addRows(1, 0, max_weight, n_items, [0], indices, weights)
    #    model.writeModel("model_highs.lp")
    model.run()

    solution = model.getSolution()
    vals = np.array(solution.col_value)

    return model.getInfoValue("objective_function_value")[1], vals
