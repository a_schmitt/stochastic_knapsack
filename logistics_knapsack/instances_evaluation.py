import time

import numpy as np
import pandas as pd
from random_test_instance import UniformPackageSelectionRandomTestInstance
from solver.least_squares_solver import LeastSquaresSolver, LeastSquaresWithCISolver
from solver.robust_solver import RobustSolver

solvers_to_test = {
    "ls": LeastSquaresSolver,
    "ls_ci": LeastSquaresWithCISolver,
    "robust": RobustSolver,
}

n_items = 60
n_packages = 1000
capacity = 40
test_bed = {
    "uniform": [
        (
            UniformPackageSelectionRandomTestInstance(n_items, n_packages, seed=i),
            capacity,
        )
        for i in range(100)
    ],
    "poisson": [
        (
            UniformPackageSelectionRandomTestInstance(n_items, n_packages, seed=i),
            capacity,
        )
        for i in range(100)
    ],
}

results = []
for name, test_bed in test_bed.items():
    for solver_name, solver in solvers_to_test.items():
        print(f"Testing {solver_name} on {name}")
        feasibles = np.zeros(len(test_bed), dtype=bool)
        gaps = np.zeros(len(test_bed))
        # time this loop
        start = time.time()
        for i, (instance, capacity) in enumerate(test_bed):
            obj, solution, estimated_feasibility = solver().solve(instance, capacity)
            feasible = instance.solution_is_feasible(solution, capacity)
            gap = instance.solution_optimality_gap(solution, capacity)
            feasibles[i] = feasible
            gaps[i] = gap
        print(f"Feasible: {sum(feasibles)/len(feasibles)}")
        print(f"Gap: {gaps.mean()}")
        gaps[gaps < 0] = 1
        duration = time.time() - start
        results.append(
            {
                "name": name,
                "solver": solver_name,
                "feasible": sum(feasibles) / len(feasibles),
                "gap": gaps.mean(),
                "time": duration,
            }
        )

print(pd.DataFrame(results))
print(pd.DataFrame(results).to_markdown())
