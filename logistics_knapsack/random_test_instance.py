# this class generates test instances for the Christmas present problem
# given as input are a number of items as well as a random seed and a number of combinations of items that we have measured.
# the items are generated randomly with the given seed

import numpy as np
from knapsack_solver.knapsack_solver import solve_knapsack_highs


class RandomTestInstance:
    def __init__(
        self,
        n_items: int,
        n_packages: int,
        max_item_volume: int = 30,
        max_item_price=120,
        measurement_variance: float = 2,
        seed: int = 42,
    ):
        self.n_items = n_items
        self.n_packages = n_packages
        self.seed = seed
        self.random = np.random.RandomState(seed)
        self.max_item_volume = max_item_volume
        self.max_item_price = max_item_price
        self.measurement_variance = measurement_variance

        self.item_volumes, self.item_prices = self.generate_items()
        (
            self.packages,
            self.package_volumes,
            self.measured_package_volumes,
        ) = self.generate_packages()

    # generate random items
    def generate_items(self):
        item_volumes = self.random.randint(1, self.max_item_volume, size=self.n_items)
        item_prices = self.random.randint(20, self.max_item_price, size=self.n_items)
        return item_volumes, item_prices

    def generate_packages(self):
        packages = self.generate_package_selection()
        package_volumes = packages @ self.item_volumes
        measured_package_volumes = package_volumes + self.random.normal(
            0, np.sqrt(self.measurement_variance), self.n_packages
        )
        # clip the weighted package volumnes by 0
        measured_package_volumes.clip(min=0)

        return packages, package_volumes, measured_package_volumes

    def generate_package_selection(self):
        return np.zeros(shape=(self.n_packages, self.n_items))

    def get_optimal_items(self, max_volume):
        return solve_knapsack_highs(self.item_volumes, self.item_prices, max_volume)

    def solution_is_feasible(self, x: np.array, max_volume: int):
        return sum(self.item_volumes * x) <= max_volume

    def solution_feasibility_gap(self, x: np.array, max_volume: int):
        return (sum(self.item_volumes * x) - max_volume) / max_volume

    def solution_optimality_gap(self, x: np.array, b: int):
        optimal_solution_value, _ = self.get_optimal_items(b)
        solution_value = sum(self.item_prices * x)
        return (optimal_solution_value - solution_value) / optimal_solution_value


class UniformPackageSelectionRandomTestInstance(RandomTestInstance):
    def generate_package_selection(self):
        # generate a random matrix with zero and ones
        return self.random.randint(2, size=(self.n_packages, self.n_items))


class PoissonPackageSelectionRandomTestInstance(RandomTestInstance):
    def generate_package_selection(self):
        n_packages_per_item = np.random.poisson(68, self.n_items)
        packages = np.zeros(shape=(self.n_packages, self.n_items))
        for i in range(self.n_items):
            packages_choosen = np.random.choice(
                self.n_packages, n_packages_per_item[i], replace=False
            )
            packages[packages_choosen, i] = 1
        return packages
